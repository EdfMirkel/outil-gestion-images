module com.example.outil_gestion_images {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires net.synedra.validatorfx;
    requires org.kordamp.bootstrapfx.core;
    requires com.almasb.fxgl.all;


    opens com.example.outil_gestion_images.view to javafx.fxml;
    exports com.example.outil_gestion_images.view to javafx.fxml;
    opens com.example.outil_gestion_images to javafx.fxml;
    exports com.example.outil_gestion_images;
    exports com.example.outil_gestion_images.controller;
    opens com.example.outil_gestion_images.controller to javafx.fxml;


}