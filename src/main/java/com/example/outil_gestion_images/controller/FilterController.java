package com.example.outil_gestion_images.controller;

import com.example.outil_gestion_images.filters.RGBSwapFilter;
import com.example.outil_gestion_images.filters.GrayscaleFilter;
import com.example.outil_gestion_images.filters.ImageFilter;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class FilterController {
    private final ImageView imageView;
    private Image selectedImage;

    public FilterController(ImageView imageView) {
        this.imageView = imageView;
    }

    public void applyFilters() {
        ImageFilter rgbSwapFilter = new RGBSwapFilter();
        ImageFilter grayscaleFilter = new GrayscaleFilter();

        Image rgbSwappedImage = rgbSwapFilter.apply(selectedImage);
        Image grayscaleImage = grayscaleFilter.apply(selectedImage);


        imageView.setImage(rgbSwappedImage);
       imageView.setImage(grayscaleImage);
    }

    public void setSelectedImage(Image selectedImage) {
        this.selectedImage = selectedImage;
    }
}
