package com.example.outil_gestion_images.controller;

import com.example.outil_gestion_images.model.ImageModel;
import com.example.outil_gestion_images.view.ImageViewerView;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;

import java.io.File;

public class ImageController {
    private ImageModel model;
    private ImageViewerView view;

    public ImageController(ImageModel model, ImageViewerView view) {
        this.model = model;
        this.view = view;
    }

    public void initController() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Image File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        File selectedFile = fileChooser.showOpenDialog(view.getStage());
        if (selectedFile != null) {
            Image image = new Image(selectedFile.toURI().toString());
            model.setOriginalImage(image);
            view.getImageView().setImage(image);
        }
    }

}
