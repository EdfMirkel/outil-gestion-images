package com.example.outil_gestion_images;

import com.example.outil_gestion_images.controller.FilterController;
import com.example.outil_gestion_images.controller.ImageController;
import com.example.outil_gestion_images.filters.GrayscaleFilter;
import com.example.outil_gestion_images.filters.ImageFilter;
import com.example.outil_gestion_images.filters.RGBSwapFilter;
import com.example.outil_gestion_images.model.ImageModel;
import com.example.outil_gestion_images.view.ImageViewerView;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class MainApplication extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/example/outil_gestion_images/image-view.fxml"));
        Parent root = loader.load();

        ImageViewerView view = loader.getController();
        ImageModel model = new ImageModel();
        ImageController controller = new ImageController(model, view);

        view.setStage(stage);
        controller.initController();

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

        try {
//            ImageView view2 = loader.getController();
//
//            FilterController controller2 = new FilterController(view2);

            System.out.println("j'ai entrer en try");
//        Image originalImage = new Image("/resources/image.png");
//        ImageView imageView = new ImageView();
//        ImageFilter rgbSwapFilter = new RGBSwapFilter();
//        ImageFilter grayscaleFilter = new GrayscaleFilter();
//
//        Image rgbSwappedImage = rgbSwapFilter.apply(originalImage);
//        imageView.setImage(rgbSwappedImage);
//
//        Image grayscaleImage = grayscaleFilter.apply(originalImage);
//        imageView.setImage(grayscaleImage);

        }catch (NullPointerException e){
            System.out.println("Image not found: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        launch(args);

    }
}
