package com.example.outil_gestion_images.model;

import javafx.scene.image.Image;

public class ImageModel {
    private Image originalImage;

    public void setOriginalImage(Image image) {
        this.originalImage = image;
    }

    public Image getOriginalImage() {
        return originalImage;
    }

}
