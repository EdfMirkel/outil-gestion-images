package com.example.outil_gestion_images.view;

import com.example.outil_gestion_images.filters.ImageFilter;
import com.example.outil_gestion_images.filters.RGBSwapFilter;
import com.example.outil_gestion_images.filters.GrayscaleFilter;
import com.example.outil_gestion_images.filters.SepiaFilter;
import com.example.outil_gestion_images.filters.SobelFilter;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class ImageViewerView {
    @FXML
    private ImageView imageView;
    @FXML
    private Button rotateButton;
    @FXML
    private Button flipHorizontalButton;
    @FXML
    private Button flipVerticalButton;

    private Stage stage;
    private Image currentImage;
    private final ImageFilter rgbSwapFilter = new RGBSwapFilter();
    private final ImageFilter grayscaleFilter = new GrayscaleFilter();
    private final ImageFilter sepiaFilter = new SepiaFilter();
    private final ImageFilter sobelFilter = new SobelFilter();

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Stage getStage() {
        return stage;
    }

    public ImageView getImageView() {
        return imageView;
    }

    @FXML
    private void rotateImage() {
        if (imageView.getImage() != null) {
            imageView.setRotate(imageView.getRotate() + 90);
        }
    }

    @FXML
    private void flipHorizontal() {
        if (imageView.getImage() != null) {
            imageView.setScaleX(imageView.getScaleX() * -1);
        }
    }

    @FXML
    private void flipVertical() {
        if (imageView.getImage() != null) {
            imageView.setScaleY(imageView.getScaleY() * -1);
        }
    }

    public void setCurrentImage(Image image) {
        if (image == null) {
            throw new IllegalArgumentException("image cannot be null");
        }
        this.currentImage = image;
        imageView.setImage(currentImage);
    }

    @FXML
    private void applyRGBSwapFilter() {
        Image rgbSwappedImage = rgbSwapFilter.apply(currentImage);
        setCurrentImage(rgbSwappedImage);
        imageView.setImage(rgbSwappedImage);
    }

    @FXML
    private void applyGrayscaleFilter() {
        Image grayscaleImage = grayscaleFilter.apply(currentImage);
        setCurrentImage(grayscaleImage);
        imageView.setImage(grayscaleImage);
    }

    @FXML
    private void applySepiaFilter() {
        Image sepiaImage = sepiaFilter.apply(currentImage);
        imageView.setImage(sepiaImage);
    }

    @FXML
    private void applySobelFilter() {
        Image sobelImage = sobelFilter.apply(currentImage);
        imageView.setImage(sobelImage);
    }

}
