package com.example.outil_gestion_images.filters;

import javafx.scene.image.Image;

public interface ImageFilter {
    Image apply(Image inputImage);
}
