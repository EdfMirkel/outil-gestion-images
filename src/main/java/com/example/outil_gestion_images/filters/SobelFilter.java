package com.example.outil_gestion_images.filters;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

public class SobelFilter extends AbstractImageFilter {
    @Override
    public Image apply(Image inputImage) {
        if (inputImage != null) {
            WritableImage writableImage = convertToWritableImage(inputImage);
            PixelWriter pixelWriter = writableImage.getPixelWriter();
            PixelReader pixelReader = inputImage.getPixelReader();

            int width = (int) inputImage.getWidth();
            int height = (int) inputImage.getHeight();

            int[][] GX = {
                    {-1, 0, 1},
                    {-2, 0, 2},
                    {-1, 0, 1}
            };

            int[][] GY = {
                    {-1, -2, -1},
                    {0, 0, 0},
                    {1, 2, 1}
            };

            for (int y = 1; y < height - 1; y++) {
                for (int x = 1; x < width - 1; x++) {
                    double sumX = 0;
                    double sumY = 0;
                    double sum = 0;

                    for (int i = -1; i <= 1; i++) {
                        for (int j = -1; j <= 1; j++) {
                            Color color = pixelReader.getColor(x + i, y + j);
                            double gray = (color.getRed() + color.getGreen() + color.getBlue()) / 3;
                            sumX += gray * GX[i + 1][j + 1];
                            sumY += gray * GY[i + 1][j + 1];
                        }
                    }

                    sum = Math.sqrt(sumX * sumX + sumY * sumY);
                    sum = Math.min(Math.max(sum, 0), 1);

                    pixelWriter.setColor(x, y, new Color(sum, sum, sum, 1));
                }
            }
            return writableImage;
        } else {

            return null;
        }
    }
}