package com.example.outil_gestion_images.filters;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;


public class GrayscaleFilter extends AbstractImageFilter {
    @Override
    public Image apply(Image inputImage) {
        WritableImage writableImage = convertToWritableImage(inputImage);
        PixelWriter pixelWriter = writableImage.getPixelWriter();
        PixelReader pixelReader = inputImage.getPixelReader();

        int width = (int) inputImage.getWidth();
        int height = (int) inputImage.getHeight();

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                Color color = pixelReader.getColor(x, y);
                double gray = (color.getRed() + color.getGreen() + color.getBlue()) / 3;
                pixelWriter.setColor(x, y, new Color(gray, gray, gray, color.getOpacity()));
            }
        }
        return writableImage;
    }
}