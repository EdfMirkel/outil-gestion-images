package com.example.outil_gestion_images.filters;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;


abstract class AbstractImageFilter implements ImageFilter {
    protected WritableImage convertToWritableImage(Image inputImage) {
        if (inputImage == null) {
            throw new IllegalArgumentException("inputImage cannot be null");
        }
        int width = (int) inputImage.getWidth();
        int height = (int) inputImage.getHeight();
        WritableImage writableImage = new WritableImage(width, height);
        PixelReader pixelReader = inputImage.getPixelReader();
        PixelWriter pixelWriter = writableImage.getPixelWriter();

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                pixelWriter.setColor(x, y, pixelReader.getColor(x, y));
            }
        }
        return writableImage;
    }
}
