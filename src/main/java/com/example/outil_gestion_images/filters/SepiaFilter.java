package com.example.outil_gestion_images.filters;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class SepiaFilter extends AbstractImageFilter {
    @Override
    public Image apply(Image inputImage) {
        WritableImage writableImage = convertToWritableImage(inputImage);
        PixelWriter pixelWriter = writableImage.getPixelWriter();
        PixelReader pixelReader = inputImage.getPixelReader();

        int width = (int) inputImage.getWidth();
        int height = (int) inputImage.getHeight();

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                Color color = pixelReader.getColor(x, y);
                double red = (color.getRed() * 0.393) + (color.getGreen() * 0.769) + (color.getBlue() * 0.189);
                double green = (color.getRed() * 0.349) + (color.getGreen() * 0.686) + (color.getBlue() * 0.168);
                double blue = (color.getRed() * 0.272) + (color.getGreen() * 0.534) + (color.getBlue() * 0.131);

                red = Math.min(red, 1);
                green = Math.min(green, 1);
                blue = Math.min(blue, 1);

                pixelWriter.setColor(x, y, new Color(red, green, blue, color.getOpacity()));
            }
        }
        return writableImage;
    }
}